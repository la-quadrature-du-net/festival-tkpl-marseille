1
00:00:03,020 --> 00:00:07,057
Merci beaucoup. Je suis Andrew Culp,
je vous parle depuis

2
00:00:07,081 --> 00:00:10,973
Los Angeles, je suis chercheur et auteur,
je travaille

3
00:00:10,985 --> 00:00:14,892
sur la théorie des médias, la philosophie
et les radicalités politiques.

4
00:00:14,904 --> 00:00:18,936
Je suis un des deux réalisateurs, et nous
sommes en compagnie de Thomas également, Tom ?

5
00:00:18,960 --> 00:00:22,972
Bonjour tout le monde. Je suis
chercheur post-doctorant

6
00:00:22,984 --> 00:00:26,597
à l'université Royal Holloway à Londres,

7
00:00:26,609 --> 00:00:30,504
et je fais de la recherche sur des sujets
comme le refus de la technologie,

8
00:00:30,516 --> 00:00:34,340
les pouvoirs urbains, et d'autres thèmes,
mais bien entendu je suis

9
00:00:34,350 --> 00:00:36,843
également l'un des
co-producteurs et réalisateurs

10
00:00:36,855 --> 00:00:39,500
de Machines in Flames que vous
allez visionner.

11
00:00:40,280 --> 00:00:44,609
Nous sommes enthousiastes à l'idée que notre
film soit projeté par Technopolice,

12
00:00:44,621 --> 00:00:49,452
car je crois qu'il n'y a pas mieux
pour représenter notre propre

13
00:00:49,464 --> 00:00:54,043
orientation politique, nos
inquiétudes à propos de la technologie

14
00:00:54,055 --> 00:00:59,069
et notre engagement profond en matière de travail
abolitioniste, anti-policier et anarchiste.

15
00:00:59,080 --> 00:01:02,695
Nous avons été en contact avec Félix,

16
00:01:02,707 --> 00:01:06,129
qui a aimablement aidé à
programmer la projection, et

17
00:01:06,141 --> 00:01:09,590
nous a donné une série de
questions auxquelles nous

18
00:01:09,602 --> 00:01:13,339
répondrons avec plaisir pour
aider à situer le contexte du film.

19
00:01:13,350 --> 00:01:16,405
C'est un documentaire expérimental,

20
00:01:16,417 --> 00:01:19,363
qui dure environ cinquante minutes.
Ce n'est pas seulement

21
00:01:19,375 --> 00:01:22,285
un documentaire relatant l'histoire
du CLODO, qui posait

22
00:01:22,297 --> 00:01:25,340
des bombes dans le sud de
la France dans les années 80,

23
00:01:25,350 --> 00:01:30,741
mais c'est aussi une réflexion, ou une enquête
philosophique, une étude théorique

24
00:01:30,753 --> 00:01:36,696
sur ce que cela signifie pour nous en tant
que personnes piégées dans Internet

25
00:01:36,708 --> 00:01:40,069
et dans un monde profondément
informatisé à l'heure actuelle,

26
00:01:40,094 --> 00:01:42,995
et une opportunité de réfléchir

27
00:01:43,007 --> 00:01:48,533
aux enjeux du sabotage et du refus
de la technologie aujourd'hui.

28
00:01:50,730 --> 00:01:53,909
La première question qui nous
a été posée était :

29
00:01:53,980 --> 00:01:57,478
« Comment l'idée du film a pris naissance ? »,
et peut-être que Thomas peut nous

30
00:01:57,490 --> 00:02:01,219
en dire un peu plus sur le déclic qui
nous a mené à ce projet.

31
00:02:01,700 --> 00:02:05,829
Bien sûr, oui. En premier lieu
nous sommes tombés

32
00:02:05,840 --> 00:02:10,312
sur une entrevue, ou plutôt ce qui
était sensé être une auto-entrevue

33
00:02:10,324 --> 00:02:14,880
qui est parue dans un journal
étasunien du nom de Processed World,

34
00:02:14,892 --> 00:02:19,476
mais qui fut initialement publiée
en France dans la revue Terminal en 1983.

35
00:02:19,488 --> 00:02:24,300
Et dans cette entrevue, le CLODO parle des
raisons qui les ont conduit à leurs attaques.

36
00:02:24,540 --> 00:02:29,228
Je pense que ces personnes débattaient
autour du contexte technologique

37
00:02:29,240 --> 00:02:34,209
dans lequel elles opéraient, et ce que
nous avons trouvé intéressant à propos d'elles

38
00:02:34,221 --> 00:02:38,948
est qu'à la même époque, d'autres mouvements
radicaux se faisaient connaître en Europe,

39
00:02:38,960 --> 00:02:43,969
des mouvements militants comme
Action Directe ou d'autres,

40
00:02:43,980 --> 00:02:47,879
qui se sont aussi attaqués à des
infrastructures numériques et informatiques.

41
00:02:47,891 --> 00:02:52,658
Mais plusieurs choses faisaient
que le CLODO sortait du lot.

42
00:02:52,670 --> 00:02:57,095
Premièrement, le nom, qui comme vous le savez
désigne les personnes sans-abri en français,

43
00:02:57,107 --> 00:03:01,360
c'est une sorte de jeu de mots. Et le langage
utilisé durant toute l'entrevue

44
00:03:01,370 --> 00:03:05,714
est vraiment très espiègle. Cela les
démarque déjà du ton sérieux

45
00:03:05,726 --> 00:03:07,787
des collectifs comme Action Directe en France

46
00:03:07,799 --> 00:03:10,339
ou les Brigades Rouges en Italie.

47
00:03:10,350 --> 00:03:14,592
Il nous semble aussi que le collectif
avait une vision très nette

48
00:03:14,604 --> 00:03:19,252
de l'époque <i>cybernétique</i> dans
laquelle il s'inscrivait,

49
00:03:19,264 --> 00:03:24,105
et le panel des trajectoires futures possibles
que cela ouvrait au niveau sociétal.

50
00:03:24,117 --> 00:03:28,810
Nous avons aussi trouvé intéressant le fait que,

51
00:03:28,820 --> 00:03:33,129
contrairement à ces autres mouvements,

52
00:03:33,200 --> 00:03:38,962
le CLODO a disparu trois ans plus tard,
sans qu'aucun des membres ne soit découvert.

53
00:03:38,974 --> 00:03:44,823
C'est ce qui nous a mené à nous interroger :
Qu'est-devenu le CLODO ? Pourquoi a-t-il disparu ?

54
00:03:44,835 --> 00:03:50,258
Est-ce que ses membres sont encore là ?
Et c'est ce genre de questions qui nous

55
00:03:50,270 --> 00:03:56,470
a conduit à enquêter sur le CLODO.
C'est de là que le projet est parti.

56
00:03:56,710 --> 00:04:01,245
Je pense que nous avons des affinités
personnelles avec les médias

57
00:04:01,257 --> 00:04:05,488
qui publiaient leurs communiqués.
Terminal étant une revue

58
00:04:05,500 --> 00:04:09,373
très intéressée par le futur de
l'informatique, mais aussi

59
00:04:09,398 --> 00:04:14,290
à même de la critiquer et de la remettre en question,
et de faire une certaine critique de la technologie.

60
00:04:14,300 --> 00:04:17,629
Et Processed World était un magazine anti-travail

61
00:04:17,641 --> 00:04:21,286
diffusé autour de la baie de San Francisco.

62
00:04:21,298 --> 00:04:25,053
Et nous avons eu l'opportunité de communiquer

63
00:04:25,065 --> 00:04:28,601
avec certaines personnes derrière Processed World,

64
00:04:28,613 --> 00:04:32,192
qui sont des marxistes, communistes, autonomistes,

65
00:04:32,204 --> 00:04:35,871
qui ne sont pas intéressées par les partis ni par
l'idée d'en fonder un,

66
00:04:35,883 --> 00:04:39,583
mais qui réclament une manière plus
inventive, plus créative

67
00:04:39,595 --> 00:04:43,230
et plus réfléchie de vivre.
Et nous nous sommes dit que

68
00:04:43,240 --> 00:04:45,864
le CLODO en tant que groupe mettait réellement

69
00:04:45,876 --> 00:04:48,175
l'accent sur cette mouvance politique

70
00:04:48,187 --> 00:04:52,909
que nous voulions découvrir, en particulier au
regard de leur insaisissabilité et de leur nature cachée.

71
00:04:53,070 --> 00:04:59,030
La seconde question était :
« Où avons nous trouvé la plupart de informations sur le CLODO ? »

72
00:04:59,384 --> 00:05:08,230
Et je pense que Thomas qui s'est beaucoup occupé de
ces recherches est bien placé pour répondre.

73
00:05:09,019 --> 00:05:12,420
Nous avons déjà mentionné Processed World,

74
00:05:12,432 --> 00:05:16,000
où nous avons découvert le collectif,

75
00:05:16,012 --> 00:05:19,915
et quelques informations fondamentales.
Mais lorsque que nous avons

76
00:05:19,927 --> 00:05:23,861
cherché sur Internet, nous nous sommes rendus
compte qu'il n'y avait pas grand chose.

77
00:05:23,901 --> 00:05:26,994
Il y avait bien sûr le travail de Célia Izoard,

78
00:05:27,006 --> 00:05:30,839
qui a écrit de nombreux textes sur le CLODO
en France, mais mis à part ses travaux

79
00:05:30,851 --> 00:05:35,744
et le contenu très succinct
disponible sur Wikipédia à l'époque,

80
00:05:35,771 --> 00:05:38,904
il n'y avait vraiment que très
peu d'informations disponibles.

81
00:05:39,157 --> 00:05:45,229
Par conséquent nous avons décidé
de nous rendre à Toulouse

82
00:05:45,241 --> 00:05:52,190
dans un centre d'archives dénommé CRAS
(Centre de Recherche pour l'Alternative Sociale)

83
00:05:52,700 --> 00:05:59,990
qui collecte et archive toute sorte de
contenus radicaux, d'articles

84
00:06:00,002 --> 00:06:03,471
et de publications depuis les années 60 il me semble,

85
00:06:03,483 --> 00:06:07,389
et nous avons découvert en ligne
que le CRAS conservait

86
00:06:07,401 --> 00:06:14,739
un dossier sur le CLODO avec du contenu qui
n'avait jamais été numérisé ni mis en ligne.

87
00:06:14,751 --> 00:06:21,849
Nous sommes donc allés voir ces personnes qui ont
aimablement partagé ce contenu avec nous,

88
00:06:21,860 --> 00:06:27,628
et nous avons commencé à avoir une meilleure idée
sur comment les médias de l'époque traitaient

89
00:06:27,652 --> 00:06:34,010
les actions du CLODO. Nous avons également eu
l'opportunité de parler aux personnes du centre,

90
00:06:34,035 --> 00:06:39,370
qui étaient elles-mêmes actives à l'époque,
et nous ont donné un aperçu

91
00:06:39,950 --> 00:06:45,134
des autres mouvements politiques et
de la situation de l'époque,

92
00:06:45,159 --> 00:06:52,279
comment l'action directe était devenue à cette époque
comme une forme légitime d'exercice politique.

93
00:06:52,699 --> 00:06:56,777
Et de cet espace, ce lieu d'archivage,

94
00:06:56,789 --> 00:07:00,179
nous avons rencontré un réseau de gens,

95
00:07:00,190 --> 00:07:10,763
à Toulouse et ailleurs, qui ont, au moins
en partie, tiré inspiration du CLODO.

96
00:07:10,775 --> 00:07:14,258
C'est ce réseau élargi de personnes
qui nous a aidé à comprendre le CLODO

97
00:07:14,270 --> 00:07:21,305
son mode de fonctionnement, son
terrain d'action, ses méthodes ...

98
00:07:21,317 --> 00:07:24,794
Mais ce n'était pas très fourni,
il faut bien le reconnaître,

99
00:07:24,806 --> 00:07:28,874
et cela transparaît dans le film.
Il nous a fallu beaucoup de travail,

100
00:07:28,899 --> 00:07:32,127
car le CLODO était vraiment très très porté

101
00:07:32,152 --> 00:07:37,898
sur l'anonymat de ses membres, et sur
l'effacement de leurs traces à tout prix.

102
00:07:38,045 --> 00:07:40,885
C'est quelque chose, je pense,
qu'il faut garder en tête

103
00:07:40,910 --> 00:07:45,405
quand on se demande comment quelqu'un en vient
à bien connaître un groupe comme le CLODO.

104
00:07:45,880 --> 00:07:49,464
L'un des concepts les plus importants
qui transparaît dans le film

105
00:07:49,476 --> 00:07:52,146
est le questionnement de cette pulsion policière

106
00:07:52,171 --> 00:07:56,641
et la manière dont les ordinateurs
permettent une sorte d'analyse judiciaire

107
00:07:56,653 --> 00:08:00,160
très similaire à la forme que revêt
une enquête policière

108
00:08:00,170 --> 00:08:04,185
et c'est potentiellement effrayant,
car c'est une manière de fonctionner

109
00:08:04,210 --> 00:08:11,408
très procédurière et inquisitrice, qui
isole les individus, qui cartographie

110
00:08:11,420 --> 00:08:14,220
leurs interactions, et peut être utilisée

111
00:08:14,245 --> 00:08:19,045
de manière policière, à la fois par l'État,
mais aussi par d'autres personnes.

112
00:08:19,070 --> 00:08:23,224
Et en faisant ce travail, nous ne voulions surtout pas

113
00:08:23,249 --> 00:08:26,224
nous livrer outre mesure à cette pulsion inquisitrice,

114
00:08:26,249 --> 00:08:29,831
car nous ne voulions pas,
ou plutôt nous n'étions pas

115
00:08:29,856 --> 00:08:35,439
très intéressés par les identités légales
réelles des membres du CLODO,

116
00:08:35,464 --> 00:08:37,612
puisqu'en réalité nous voulions
protéger ces personnes,

117
00:08:37,637 --> 00:08:42,448
nous ne voulions pas faire le travail
de la police à leur place.

118
00:08:42,460 --> 00:08:47,193
ce qui a donné ce profond
questionnement conceptuel et artistique

119
00:08:47,218 --> 00:08:50,900
sur comment en apprendre plus sur le CLODO,
voire nous mettre à leur place

120
00:08:50,925 --> 00:08:55,098
sans devenir nous-mêmes « la police ».

121
00:08:55,123 --> 00:08:59,144
Cela signifie que nous devions apprendre de l'histoire
de Toulouse, aucun d'entre nous n'étant français,

122
00:08:59,156 --> 00:09:06,062
et je présume que nous ne serons
jamais aussi précis ou informés qu'il le faudrait,

123
00:09:06,350 --> 00:09:10,076
mais nous y avons découvert un mouvement
écologiste en plein essor à la fin des années 70,

124
00:09:10,088 --> 00:09:15,383
qui établira un modèle pour
l'action directe et l'action écologiste,

125
00:09:15,442 --> 00:09:19,821
nous savons qu'il y a un héritage radical très
ancré dans le sud de la France

126
00:09:19,846 --> 00:09:25,463
lié au communisme, à l'antifascisme,
à l'anarchisme, à l'hérésie*,

127
00:09:19,846 --> 00:09:25,463
{\an8}(*ndt : l'hérésie cathare)

128
00:09:25,488 --> 00:09:30,048
mais aussi que les enjeux sont
un peu difficiles à saisir parfois,

129
00:09:30,073 --> 00:09:35,240
étant donné que c'est une ville à gouvernance
socialiste d'après-guerre se modernisant,

130
00:09:35,278 --> 00:09:38,743
et le fait que Toulouse soit un pôle
scientifique et mathématique,

131
00:09:38,768 --> 00:09:44,959
qui accueille Airbus et d'autres
grandes usines et industries.

132
00:09:44,984 --> 00:09:49,399
Nous avons découvert que le CLODO a fait
sa propre, sorte de "cartographie" des lieux,

133
00:09:49,424 --> 00:09:54,850
et une critique incisive pour prendre parti
dans le milieu radical de l'époque.

134
00:09:54,875 --> 00:10:02,180
Et c'est ce qui nous a intrigué, malgré le fait
que nous soyons étrangers aux préoccupations locales.

135
00:10:03,010 --> 00:10:07,076
La dernière question, que nous apprécions beaucoup,

136
00:10:07,101 --> 00:10:12,061
et que nous pensons être une question ouverte
à laquelle nous n'avons pas tellement de réponses

137
00:10:12,086 --> 00:10:16,567
mais plutôt d'autres questions ou
problématiques nous-mêmes,

138
00:10:16,592 --> 00:10:20,805
est : « Comment le film se rattache-t-il
 aux luttes contemporaines

139
00:10:20,830 --> 00:10:23,765
contre la surveillance informatique
et l'automatisation de masse ?

140
00:10:23,790 --> 00:10:28,003
Quel est l'héritage du CLODO, et
que devons-nous en penser aujourd'hui ? »

141
00:10:28,028 --> 00:10:30,348
Tu aimerais dire quelque chose
 là dessus Thomas ?

142
00:10:30,373 --> 00:10:35,413
Oui, bien sûr. Je pense, et cela
nous ramène au questionnement initial

143
00:10:35,438 --> 00:10:40,134
à propos de ce qui nous a parlé chez le CLODO
et ce pourquoi nous nous y sommes intéressés.

144
00:10:40,159 --> 00:10:44,998
C'est, tout du moins dans
le contexte du Royaume-Uni

145
00:10:45,022 --> 00:10:50,996
dans lequel j'évolue à la fois
professionnellement et politiquement,

146
00:10:53,728 --> 00:10:56,623
c'est comment appréhender
le changement technologique

147
00:10:56,648 --> 00:10:58,849
au travers du prisme de la réforme.

148
00:10:58,874 --> 00:11:07,368
Les ONG mais aussi des groupes
plus radicaux mettent l'accent

149
00:11:07,393 --> 00:11:14,770
sur la législation, ou demandent la
nationalisation d'entreprises comme Facebook,

150
00:11:14,794 --> 00:11:21,063
ou une remise en question de comment

151
00:11:21,087 --> 00:11:24,930
l'intelligence artificielle s'approprie certains
types de données et se les accapare.

152
00:11:26,902 --> 00:11:31,755
Nous sommes actuellement en train d'écrire
un papier là dessus, sur le fait que

153
00:11:31,780 --> 00:11:37,866
le CLODO, en tant que groupe ou en
tant qu'ensemble d'activistes

154
00:11:37,892 --> 00:11:42,860
s'est éloigné de cette approche
techno-réformiste qui...

155
00:11:42,870 --> 00:11:44,010
...

156
00:11:44,120 --> 00:11:49,270
... qui tente d'imaginer
des futurs radicaux

157
00:11:49,630 --> 00:11:56,470
à travers de petites gentilles avancées dans le temps.

158
00:11:56,480 --> 00:12:01,339
Et je pense que le CLODO, sur cet aspect, est ...
s'il existe un héritage direct du CLODO

159
00:12:01,351 --> 00:12:07,367
aujourd'hui alors il ne se trouve pas dans les
approches légalistes et réformistes

160
00:12:07,392 --> 00:12:08,674
au changement technologique.

161
00:12:08,698 --> 00:12:15,910
Malgré cela, il y a aujourd'hui
nombre d'acteurs actifs qui

162
00:12:15,920 --> 00:12:23,002
existent explicitement dans l'héritage du CLODO.

163
00:12:23,027 --> 00:12:28,400
Il y a plusieurs groupes, en France
notamment mais aussi en Allemagne,

164
00:12:28,890 --> 00:12:38,463
qui ont été impliqués dans, par exemple, vous le savez
probablement déjà, le sabotage de la fibre optique.

165
00:12:38,488 --> 00:12:42,445
C'est arrivé il y a plusieurs mois en France,
des câbles de fibre optique ont été

166
00:12:42,457 --> 00:12:46,026
sectionnés partout sur le territoire
au même moment, ce qui suggère

167
00:12:46,038 --> 00:12:50,196
qu'une attaque coordonnée a eu lieu.

168
00:12:51,123 --> 00:12:54,593
Et bien que nous ne connaissions pas les
motivations politiques de ces personnes,

169
00:12:54,618 --> 00:13:00,019
il y a une connection très distincte
autour de ces pulsions abolitionnistes

170
00:13:00,043 --> 00:13:02,043
envers les infrastructures numériques.

171
00:13:02,068 --> 00:13:06,421
Bien entendu les enjeux aujourd'hui
sont très différents n'est-ce pas ?

172
00:13:06,433 --> 00:13:11,421
Le CLODO était en mesure de se cacher,
d'effacer ses traces

173
00:13:11,446 --> 00:13:14,768
relativement facilement, sans
téléphone mobile pour les pister,

174
00:13:14,793 --> 00:13:21,301
sans même un internet public à leur disposition,
il leur était bien plus facile de couvrir leurs arrières

175
00:13:21,326 --> 00:13:23,979
par conséquent, les enjeux sont vraiment très différents

176
00:13:24,004 --> 00:13:28,821
et l'une des questions que
nous voulons poser dans le film,

177
00:13:28,845 --> 00:13:31,776
est : Est-ce que ces attaques,

178
00:13:31,800 --> 00:13:36,120
ces assauts matériels délibérés contre
les infrastructures numériques ...

179
00:13:36,145 --> 00:13:41,025
dans quelle mesure sont elles en phase avec le CLODO?

180
00:13:41,050 --> 00:13:48,717
Est-il judicieux de penser que ces
groupes, cet ensemble d'activistes,

181
00:13:48,741 --> 00:13:51,452
ce type d'attaques contre
l'informatique cybernétique,

182
00:13:51,477 --> 00:13:54,738
ressemble à ce que faisait
le CLODO à l'époque ?

183
00:13:54,763 --> 00:13:59,209
Je pense que nous avons beaucoup
de choses à apprendre

184
00:13:59,234 --> 00:14:02,041
sur le CLODO à travers ces groupes actuels.

185
00:14:02,065 --> 00:14:05,310
Et peut-être que Andrew veut
continuer sur ce point ...

186
00:14:05,320 --> 00:14:08,953
Bien sûr. Je pense que
tu as très bien résumé Thomas.

187
00:14:08,978 --> 00:14:12,249
Je voudrais aussi ajouter que nous
sommes intéressés par le sabotage

188
00:14:12,275 --> 00:14:14,122
en tant que tactique, mais aussi

189
00:14:14,154 --> 00:14:20,170
plus largement par la catégorie ou le
concept du sabotage comme étant

190
00:14:20,195 --> 00:14:24,180
une approche que nous pourrions
qualifier « d'illégalisme ».

191
00:14:24,190 --> 00:14:28,869
Des personnes qui cherchent à provoquer
des changements socio-culturels et politiques

192
00:14:28,894 --> 00:14:36,909
pas seulement en dehors des institutions politiques,
mais bien souvent en les enfreignant ou les transgressant.

193
00:14:36,934 --> 00:14:40,539
Nous connaissons déjà cela à travers l'action directe,

194
00:14:40,564 --> 00:14:42,481
mais aussi à travers d'autres formes de sabotage.

195
00:14:42,505 --> 00:14:46,032
Et c'est toute une vision du monde qui nous intéresse,

196
00:14:46,057 --> 00:14:49,624
pas seulement pour ce film, mais
que nous souhaitons continuer d'explorer.

197
00:14:49,636 --> 00:14:53,959
C'est en partie une des idées de notre collectif,
l'Internationale Destructionniste

198
00:14:53,970 --> 00:14:59,028
dans lequel nous donnons de la visibilité et de la voix
et recherchons des formes de médias

199
00:14:59,053 --> 00:15:02,397
qui correspondent à cet élan
destructionniste,

200
00:15:02,422 --> 00:15:06,446
sans pour autant être dans un monde à part,

201
00:15:06,458 --> 00:15:10,257
mais qui sont des expressions
de rage, de frustration

202
00:15:10,269 --> 00:15:15,622
et de mécontentement ...
trouver des manières pour suivre tout cela.

203
00:15:15,647 --> 00:15:18,237
Par exemple, nous sommes présentement
sur un projet de film

204
00:15:18,261 --> 00:15:21,915
dans lequel nous diffusons des vidéos
tournées au téléphone par des personnes

205
00:15:21,927 --> 00:15:26,153
travaillant dans les usines iPhone
en Inde, en Chine et à Taïwan

206
00:15:26,165 --> 00:15:34,320
qui, plutôt que de se mettre simplement en grève,
attaquent les usines, les chaînes de production

207
00:15:34,345 --> 00:15:38,719
les machines, mais aussi les postes de contrôle

208
00:15:38,744 --> 00:15:44,052
comme les centres de sécurité où
les gens doivent pointer avec leurs badges.

209
00:15:44,077 --> 00:15:46,114
Et je pense que l'illégalisme

210
00:15:46,126 --> 00:15:51,465
est un cadre essentiel pour comprendre
d'où vient, peut-être, le CLODO

211
00:15:51,490 --> 00:15:55,714
mais aussi comment ils ont donné naissance
à une nouvelle manière d'appréhender la technologie.

212
00:15:55,739 --> 00:16:01,310
Et je pense que cela se rapproche des luttes
« anti-développement »,

213
00:16:01,322 --> 00:16:05,100
que ce soit contre la gentrification et les
problèmes de logements en ville,

214
00:16:05,110 --> 00:16:11,504
ou en campagne où les gens se mobilisent
contre les extensions de trains, ou d'aéroports

215
00:16:11,529 --> 00:16:13,900
ou contre d'autres politiques de croissance.

216
00:16:16,200 --> 00:16:20,763
Oui, je pense qu'avec tout ça, nous

217
00:16:20,775 --> 00:16:28,668
avons, j'espère, donné matière
à réflexion durant le visionnage

218
00:16:28,693 --> 00:16:33,789
mais aussi par la suite au moment des discussions.
Et pour conclure, nous voulons dire

219
00:16:33,814 --> 00:16:39,552
que nous souhaitons continuer d'en apprendre
plus au travers des groupes ou individus

220
00:16:39,577 --> 00:16:43,736
qui sont passés par les mêmes
questionnements que nous,

221
00:16:43,748 --> 00:16:48,648
en particulier les personnes qui ont connu
les évènements dont nous parlons dans le film,

222
00:16:48,673 --> 00:16:52,007
car nous sommes conscients qu'en tant
que non-français,

223
00:16:52,032 --> 00:16:55,447
et n'ayant pas été actifs à cette époque,

224
00:16:55,472 --> 00:16:58,192
il y a sans doute des choses qui nous échappent.

225
00:16:58,217 --> 00:17:04,058
Alors si vous souhaitez apporter des précisions,
ou réfléchir en collaboration avec nous,

226
00:17:04,083 --> 00:17:07,712
nous en serions enchantés.
Sur notre site internet,

227
00:17:07,737 --> 00:17:12,934
vous pourrez trouver beaucoup
d'archives, et d'autres travaux

228
00:17:12,959 --> 00:17:16,993
sur lesquels nous avons travaillé,
 sur <u>machinesinflames.com</u>

229
00:17:17,018 --> 00:17:21,409
vous y trouverez également nos adresses courriel
au cas où vous souhaiteriez nous contacter.

230
00:17:21,420 --> 00:17:25,126
Bon film !

