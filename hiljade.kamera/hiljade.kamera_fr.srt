1
00:00:05,979 --> 00:00:08,536
"Près de 1000 caméras
avec reconnaissance faciale

2
00:00:08,568 --> 00:00:11,247
seront installées à Belgrade
dans les deux prochaines années,

3
00:00:11,272 --> 00:00:13,738
a annoncé le ministre de
la défense, Nebojša Stefanović."

4
00:00:13,763 --> 00:00:15,742
"Les citoyens ne savent pas

5
00:00:15,767 --> 00:00:17,680
qui sera surveillé par ces caméras,
ni pourquoi."

6
00:00:17,705 --> 00:00:19,269
{\an8}(Ministre Nebojša Stefanović)

7
00:00:17,705 --> 00:00:19,269
"...L'objectif en 2020 est d'installer

8
00:00:19,293 --> 00:00:20,903
environ 2000 caméras à Belgrade..."

9
00:00:20,928 --> 00:00:22,372
{\an8}(Directeur de la police Vladimir Rebić)

10
00:00:20,928 --> 00:00:22,372
"...sensibiliser les citoyens pour

11
00:00:22,397 --> 00:00:25,229
les dissuader de tout comportement illégal
et éviter les sanctions..."

12
00:00:25,254 --> 00:00:26,993
{\an8}(Ministre Nebojša Stefanović)

13
00:00:25,254 --> 00:00:26,993
"...il n'y aura aucune rue importante,

14
00:00:27,040 --> 00:00:29,782
aucun passage, bâtiment,
entrée ou sortie [sans surveillance]..."

15
00:00:31,294 --> 00:00:34,887
Des milliers de caméras

16
00:00:37,705 --> 00:00:40,410
{\an8}Belgrade, 2020

17
00:00:41,092 --> 00:00:42,940
Un groupe de citoyens

18
00:00:42,964 --> 00:00:45,215
met en place un site,
hiljadekamera.rs

19
00:00:45,254 --> 00:00:47,613
dans l'intention de
dénoncer les abus potentiels

20
00:00:47,640 --> 00:00:51,270
des caméras installées
sans débat public

21
00:00:51,341 --> 00:00:53,539
ni discussion à propos des conséquences

22
00:00:53,586 --> 00:00:57,115
et contre les principes dont se targuent
la loi et la Constitution.

23
00:01:08,468 --> 00:01:12,196
{\an8}(Filip Milošević, initiative hiljade.kamera)

24
00:01:08,468 --> 00:01:12,196
Ces deux caméras sur les supports horizontaux

25
00:01:12,251 --> 00:01:14,946
sont celles utilisées
spécifiquement pour la circulation

26
00:01:15,178 --> 00:01:18,670
Ici, nous avons un mélange
de caméras de circulation qui surveillent

27
00:01:18,753 --> 00:01:22,932
ces deux rues, et la troisième,
qui surveille pratiquement tout.

28
00:01:27,066 --> 00:01:31,108
Nous allons maintenant vérifier les
coordonnées GPS de ce poteau,

29
00:01:31,171 --> 00:01:33,530
en nous placant juste à côté,

30
00:01:33,572 --> 00:01:36,474
et en enregistrant l'emplacement, pour pouvoir ajouter

31
00:01:36,547 --> 00:01:40,398
ce poteau et ces caméras sur la carte plus tard.

32
00:01:41,149 --> 00:01:44,172
Nous allons maintenant mettre deux
autocollants sur le poteau.

33
00:01:44,457 --> 00:01:46,927
L'un indique que c'est
un espace public

34
00:01:46,952 --> 00:01:50,644
et informe que cet espace est sous
vidéo-surveillance,

35
00:01:50,705 --> 00:01:53,861
une information qui devrait être présente
et qui ne l'est pas.

36
00:01:54,001 --> 00:01:57,954
Nous indiquons aussi l'adresse courriel
du Ministère de l'Intérieur,

37
00:01:58,080 --> 00:02:00,392
étant donné que c'est leur
rôle de mettre ces informations.

38
00:02:01,080 --> 00:02:03,952
Le second autocollant
est un QR code

39
00:02:04,019 --> 00:02:07,632
menant à l'adresse
hiljadekamera.rs

40
00:02:25,570 --> 00:02:28,536
{\an8}(Danilo Krivokapić, Fondation SHARE)

41
00:02:25,570 --> 00:02:28,536
Ce n'est pas une vidéo-surveillance ordinaire.

42
00:02:28,601 --> 00:02:31,649
Il ne s'agit pas d'une situation
où quelque-chose s'est passé

43
00:02:31,678 --> 00:02:34,323
et la police saisit les enregistrements
pour les visionner.

44
00:02:34,348 --> 00:02:37,425
C'est de la surveillance biométrique,
qui permet de suivre n'importe-qui,

45
00:02:37,466 --> 00:02:40,669
à tout moment, dans toute la ville
pour peu qu'elle soit équipée de ce système.

46
00:02:44,767 --> 00:02:47,252
{\an8}(Milan Marinović, commissaire à la protection des données)

47
00:02:44,767 --> 00:02:47,252
C'est un fait :

48
00:02:47,277 --> 00:02:49,346
Il y a de plus en plus de caméras en ville.

49
00:02:49,371 --> 00:02:52,128
Cependant, à notre connaissance,

50
00:02:52,153 --> 00:02:56,187
elles ne sont toujours pas utilisées
pour de la reconnaissance faciale.

51
00:02:56,212 --> 00:02:58,528
Elles sont utilisées pour leur
objectif principal :

52
00:02:58,553 --> 00:03:00,395
surveiller la ciruclation
et assurer la sécurité

53
00:03:02,786 --> 00:03:05,169
{\an8}(Andrej Petrovski, Fondation SHARE)

54
00:03:02,786 --> 00:03:05,169
Nous avons suivi de près

55
00:03:05,194 --> 00:03:07,414
la question des 1000 caméras de Belgrade

56
00:03:07,439 --> 00:03:11,192
depuis que le ministre a annoncé
leur installation.

57
00:03:12,347 --> 00:03:16,746
hiljade.kamera.rs est une
communauté pour toute personne

58
00:03:16,771 --> 00:03:20,691
qui veut empêcher l'installation de
vidéo-surveillance automatisée,

59
00:03:20,740 --> 00:03:23,800
et tout le monde peut utiliser le site

60
00:03:23,855 --> 00:03:27,840
pour s'informer sur comment
cette technologie fonctionne,

61
00:03:27,894 --> 00:03:31,929
où sont placées les caméras dans la ville,
et comment rejoindre la lutte.

62
00:03:35,258 --> 00:03:38,351
La police peut surveiller des personnes

63
00:03:38,376 --> 00:03:40,272
au motif de suspicion justifiée,
c'est légitime.

64
00:03:40,302 --> 00:03:42,106
Mais si elle surveille chaque citoyen,

65
00:03:42,131 --> 00:03:45,053
et décide de ce qu'elle fera des données
a posteriori,

66
00:03:45,078 --> 00:03:48,679
cela peut complètement
changer la société

67
00:03:48,722 --> 00:03:51,092
au point que personne ne soit tenté
de faire quoi que ce soit

68
00:03:51,117 --> 00:03:53,246
qui s'oppose au gouvernement

69
00:03:56,152 --> 00:03:58,690
Côté technique, il y a plusieurs niveaux :

70
00:03:58,808 --> 00:04:02,094
D'abord, il y a ce que l'on apelle
"équipement terminal",

71
00:04:02,149 --> 00:04:05,119
ce sont les caméras installées
dans les rues de Belgrade.

72
00:04:05,174 --> 00:04:07,884
En plus des caméras, le système
constitue aussi un réseau

73
00:04:07,909 --> 00:04:10,720
qui connecte les caméras à
un point central,

74
00:04:10,778 --> 00:04:13,203
équipé d'un stockage, et d'outils d'analyse

75
00:04:13,266 --> 00:04:16,485
du flux vidéo entrant.

76
00:04:16,894 --> 00:04:21,496
Le système d'analyse comporte
un algorithme de reconnaissance faciale.

77
00:04:21,565 --> 00:04:23,683
qui quantifie chaque visage,

78
00:04:24,128 --> 00:04:27,441
cela signifie que quelqu'un
peut suivre vos mouvements,

79
00:04:27,519 --> 00:04:31,299
vos interractions sociales,
avec qui vous avez pris un café,

80
00:04:31,323 --> 00:04:33,383
à qui vous avez parlé,
chez qui vous êtes allé...

81
00:04:34,766 --> 00:04:38,211
Ce système rend possible
l'utilisation d'une photographie,

82
00:04:38,251 --> 00:04:41,726
qu'elle provienne de la base de
données, ou qu'elle soit importée

83
00:04:41,751 --> 00:04:45,256
d'un fichier externe.
Le système va la scanner,

84
00:04:45,284 --> 00:04:51,223
et indiquer là où la personne
s'est rendue.

85
00:04:53,283 --> 00:04:57,162
Ces caméras analysent
automatiquement les images

86
00:04:57,187 --> 00:05:01,256
et il existe beaucoup de manières
d'utiliser et d'abuser de cette technologie.

87
00:05:02,141 --> 00:05:06,998
{\an8}(Marija Gavrilov, podcast Exponential View)

88
00:05:02,141 --> 00:05:06,998
Sans recherche approfondie ni débat public,

89
00:05:07,115 --> 00:05:11,138
une telle chose ne devrait jamais
être implémentée.

90
00:05:11,171 --> 00:05:12,783
{\an8}(Sofija Todorović, B.I.R.N.)

91
00:05:11,171 --> 00:05:12,783
Ces technologies de surveillances

92
00:05:12,808 --> 00:05:14,855
ont été introduites complètement dans l'ombre,

93
00:05:14,880 --> 00:05:17,465
sans regard pour la loi.
C'est problématique à de nombreux égards.

94
00:05:17,490 --> 00:05:19,209
{\an8}(Slobodan Marković, Conseiller T.I.C.)

95
00:05:17,490 --> 00:05:19,209
Quels modèles d'intelligence artificielle

96
00:05:19,242 --> 00:05:21,417
ont été utilisés ici ?
Lesquels sont actifs ?

97
00:05:21,442 --> 00:05:23,398
Quels types de comportements
sont surveillés ?

98
00:05:23,423 --> 00:05:24,910
Comment sont collectées les données ?

99
00:05:24,934 --> 00:05:26,483
Combien de temps et où sont-elles conservées ?

100
00:05:26,508 --> 00:05:28,976
Quand sont-elles effacées (si elles le sont) ?

101
00:05:29,145 --> 00:05:30,974
Ce sont des questions importantes

102
00:05:30,998 --> 00:05:33,333
qui restent sans réponse.

103
00:05:33,559 --> 00:05:35,647
Cela concerne tout le monde
qui marche dans les rues,

104
00:05:35,672 --> 00:05:37,749
toutes les personnes qui
tiennent à leur vie privée,

105
00:05:37,774 --> 00:05:41,164
qui ne veulent pas que leur identité soit
utilisée à mauvais escient.

106
00:05:41,189 --> 00:05:42,958
Cela nous concerne tous & toutes.

107
00:05:50,534 --> 00:05:52,603
En tant que citoyennes & citoyens,

108
00:05:52,628 --> 00:05:54,132
il s'agit de savoir dans quel
genre de ville

109
00:05:54,157 --> 00:05:55,796
nous voulons habiter.

110
00:05:55,920 --> 00:05:57,679
Voulons-nous habiter dans une ville

111
00:05:57,704 --> 00:05:59,390
où quelqu'un surveille tous nos
faits et gestes ?

112
00:05:59,470 --> 00:06:04,211
Qui dit vidéosurveillance, dit nécessité
de protection de la vie privée,

113
00:06:04,236 --> 00:06:06,836
Et concernant la
protection de la vie privée,

114
00:06:06,861 --> 00:06:10,019
nous ne pouvons pas faire fi de la loi
sur la protection des données personnelles

115
00:06:10,056 --> 00:06:14,361
Malheureusement, cette loi ne prévoit
rien à propos de la vidéo-surveillance.

116
00:06:14,557 --> 00:06:16,545
{\an8}(Nevena Ružić, Open Society Foundations)

117
00:06:14,557 --> 00:06:16,545
Si vous voulez attenter aux droits humains

118
00:06:16,569 --> 00:06:18,822
le point de départ
c'est la Constitution,

119
00:06:18,883 --> 00:06:21,523
qui statue que les limitations
des droits humains

120
00:06:21,548 --> 00:06:24,425
doivent être prescrites par la loi,
et que la loi ne doit pas

121
00:06:24,450 --> 00:06:28,253
interférer avec l'essence même du droit
qui est limité.

122
00:06:28,278 --> 00:06:30,138
Cela signifie qu'il y a
besoin d'une loi

123
00:06:30,163 --> 00:06:32,555
qui régule ce type d'utilisation
des données personnelles.

124
00:06:32,580 --> 00:06:35,658
Cependant, la loi ne peut pas simplement
dire "des données seront traitées, et

125
00:06:35,683 --> 00:06:38,564
de la vidéo-surveillance automatisée sera
introduite partout", point barre.

126
00:06:38,589 --> 00:06:43,023
La loi doit avoir une finalité, ce pour quoi elle est
rédigée au départ,

127
00:06:43,048 --> 00:06:47,673
et ne peut pas constituer une violation
des droits humains fondamentaux,

128
00:06:47,698 --> 00:06:49,469
le cas échéant, le droit
à la vie privée.

129
00:06:49,502 --> 00:06:54,276
Le droit à la vie privée sera
menacé sans aucun doute.

130
00:06:54,390 --> 00:07:00,007
Cependant, c'est pour cela que nous
devons nous concentrer sur les mesures

131
00:07:00,032 --> 00:07:03,705
que le destinataire des données,
en l'occurence le Ministère de l'Intérieur,

132
00:07:03,730 --> 00:07:06,392
doit implémenter afin de minimiser les risques.

133
00:07:06,583 --> 00:07:11,067
Les principes à la base de notre Constitution
et de nos lois sont clairs.

134
00:07:11,127 --> 00:07:16,773
L'utilisation de ce système n'est possible
que si elle est nécessaire et proportionnée.

135
00:07:16,876 --> 00:07:19,595
Je pense qu'aucune de ces conditions n'est remplie

136
00:07:19,628 --> 00:07:23,638
et que le système comporte bien plus de
risques et de menaces aux

137
00:07:23,663 --> 00:07:28,055
libertés individuelles et aux droits que de
bénéfices en matière de sécurité publique.

138
00:07:32,405 --> 00:07:36,758
{\an8}(Saša Đorđević, Centre pour la politique
de sécurité de Belgrade)

139
00:07:32,405 --> 00:07:36,758
Le ministre de l'intérieur a déjà affirmé que Belgrade
était une ville plus sûre que

140
00:07:36,824 --> 00:07:41,734
bien d'autres villes européennes, tout en
soutenant en parralèle

141
00:07:41,797 --> 00:07:46,051
que la vidéo-surveillance était
introduite pour améliorer la sécurité.

142
00:07:46,181 --> 00:07:49,727
Si l'objectif principal
est de réduire la criminalité

143
00:07:49,815 --> 00:07:53,922
d'arrêter les criminels,
en particulier les "plus dangereux",

144
00:07:53,957 --> 00:07:56,762
dans ce cas on devrait considérer

145
00:07:56,803 --> 00:08:00,105
que les images des caméras
ne soient comparées

146
00:08:00,138 --> 00:08:04,871
qu'aux bases de données criminelles,
comme c'est le cas dans certains pays.

147
00:08:05,093 --> 00:08:10,087
La recherche sur l'impact de la
vidéo-surveillance en matière de sécurité

148
00:08:10,112 --> 00:08:12,712
a principalement été menée
dans des pays d'Europe de l'Ouest.

149
00:08:12,798 --> 00:08:16,563
Le consensus tend à démontrer
que la vidéo-surveillance

150
00:08:16,588 --> 00:08:21,873
a un impact préventif en matière
de sécurité routière.

151
00:08:21,965 --> 00:08:25,887
Elle peut également aider à
réduire les délits contre les biens.

152
00:08:25,926 --> 00:08:31,649
Cependant, elle n'a qu'un très
faible impact sur la criminalité.

153
00:08:33,818 --> 00:08:36,784
Une caméra "intelligente"
est totalement impersonnelle.

154
00:08:36,808 --> 00:08:42,504
Elle surveille à la fois les personnes criminelles,
et celles qui ne le sont pas.

155
00:08:42,549 --> 00:08:47,510
Et si le postulat est que tout le monde
est criminel, dans ce cas

156
00:08:47,545 --> 00:08:50,658
nous pouvons aisément justifier
la nécessité de surveiller.

157
00:09:10,560 --> 00:09:12,640
{\an8}Ella Jakubowska - EDRi

158
00:09:10,560 --> 00:09:12,640
Les changements de comportement des personnes

159
00:09:12,920 --> 00:09:14,720
lorsqu'elles sont constamment surveillées

160
00:09:15,000 --> 00:09:16,920
sont très documentés

161
00:09:17,240 --> 00:09:20,720
et si l'on extrapole cela à un niveau sociétal

162
00:09:21,040 --> 00:09:25,320
et que l'on pense à la manière dont nous pourrions
collectivement changer de comportement

163
00:09:25,640 --> 00:09:27,440
si nous nous savons sous surveillance...

164
00:09:27,760 --> 00:09:30,560
ça n'implique pas que notre comportement
change en mal

165
00:09:30,960 --> 00:09:32,680
mais plutôt que nous deviendrions d'un coup

166
00:09:33,040 --> 00:09:34,953
très alertes sur ce que nous faisons.

167
00:09:34,978 --> 00:09:38,433
C'est là que peut survenir un effet d'inhibition

168
00:09:38,458 --> 00:09:40,680
parce que si nous avons soudainement conscience

169
00:09:40,705 --> 00:09:43,567
que des caméras nous suivent en permanence

170
00:09:43,592 --> 00:09:46,449
et que cela pourrait être utilisé contre nous [...]

171
00:09:48,533 --> 00:09:51,657
Le peuple doit pouvoir contrôler les
actions du gouvernement à tout moment

172
00:09:51,682 --> 00:09:55,531
Je paie des impôts dans cette ville,
et ça n'a aucun sens d'acheter

173
00:09:55,556 --> 00:09:59,896
des caméras et du matériel
qui sera utilisé pour nous surveiller

174
00:09:59,914 --> 00:10:04,045
Le processus doit être renversé : nous
devons surveiller ce que fait le gouvernement,

175
00:10:04,070 --> 00:10:08,977
et le contrôler, comme le
veut la démocratie.

176
00:10:14,399 --> 00:10:18,925
À propos de valeurs, nous entrons maintenant
dans l'ère de la surveillance de masse,

177
00:10:18,950 --> 00:10:22,356
ce qui signifie qu'en permanence,
partout, tout est surveillé.

178
00:10:22,773 --> 00:10:24,850
[...] il y a un véritable sentiment d'émancipation

179
00:10:24,875 --> 00:10:27,557
dans le fait de pouvoir s'exprimer différemment

180
00:10:27,582 --> 00:10:30,455
et si l'on nous contraint brusquement à nous conformer

181
00:10:30,480 --> 00:10:34,137
cela représente une réelle menace
pour notre identité,

182
00:10:34,162 --> 00:10:37,208
cela affecte vraiment notre dignité,

183
00:10:37,233 --> 00:10:41,970
la personne que nous sommes,
celle qu'on nous autorise à être

184
00:10:42,197 --> 00:10:45,783
dans la société, d'une manière très dangereuse.

185
00:10:45,808 --> 00:10:48,731
Par conséquent le véritable problème pour nous

186
00:10:48,756 --> 00:10:52,122
est que les personnes qui ont déjà un
pouvoir disproportionné

187
00:10:52,147 --> 00:10:54,920
en obtiendraient encore davantage,

188
00:10:54,945 --> 00:11:01,135
et les personnes vulnérables le
deviendraient davantage,

189
00:11:01,160 --> 00:11:06,400
à travers ce schéma de "qui surveille/
qui est sous surveillance"

190
00:11:07,840 --> 00:11:10,082
Quand la vidéo-surveillance
est introduite,

191
00:11:10,115 --> 00:11:12,871
c'est très important d'évaluer les résultats

192
00:11:12,929 --> 00:11:14,796
de l'implémentation d'une telle
surveillance.

193
00:11:14,851 --> 00:11:19,491
Car si le taux de criminalité n'a pas réduit,

194
00:11:19,538 --> 00:11:24,330
si le nombre d'infractions routières n'a pas réduit,
etc.

195
00:11:25,285 --> 00:11:27,462
cela signifie que la surveillance
n'est pas efficace.

196
00:11:27,487 --> 00:11:31,026
La vidéo-surveillance avec
reconnaissance faciale

197
00:11:31,072 --> 00:11:33,599
est sans doute la plus stricte
et la plus invasive

198
00:11:33,645 --> 00:11:38,121
de toutes les mesures en matière de
protection de la vie privée.

199
00:11:41,092 --> 00:11:44,885
Lors du tournage, nous avons demandé à des
représentant·e·s du Ministère de l'Intérieur

200
00:11:44,917 --> 00:11:47,520
d'exprimer leur point de vue sur
la vidéo-surveillance automatisée à Belgrade,

201
00:11:47,545 --> 00:11:49,445
mais nous n'avons pas eu de réponse.

202
00:11:51,082 --> 00:11:53,854
Avant toute chose,
nous souhaitons débattre

203
00:11:53,886 --> 00:11:57,519
des avantages et des inconvénients
de ce système,

204
00:11:57,644 --> 00:11:59,081
ainsi que des abus potentiels,

205
00:11:59,106 --> 00:12:02,828
et en déduire si nous en
avons réellement besoin.

206
00:12:03,200 --> 00:12:05,861
[...] c'est un réseau complexe formé par les différents acteurs

207
00:12:05,886 --> 00:12:08,015
qui accroissent leur pouvoir

208
00:12:08,040 --> 00:12:11,760
au détriment des citoyennes & citoyens.

209
00:12:13,092 --> 00:12:17,021
Si les gens deviennent léthargiques
et, se savant sous surveillance,

210
00:12:17,045 --> 00:12:19,685
décident de ne plus protester,

211
00:12:19,710 --> 00:12:22,412
si personne ne veut se rendre en manifestation
à cause des caméras,

212
00:12:22,437 --> 00:12:25,718
ou si personne ne critique le gouvernement,

213
00:12:25,772 --> 00:12:28,261
il n'y a pas de progrès sans critique,

214
00:12:28,286 --> 00:12:30,577
et nous courons vers une
société totalitaire,

215
00:12:30,602 --> 00:12:35,310
ce dont je ne veux ni pour cette ville,
ni pour les générations futures

216
00:12:36,482 --> 00:12:38,708
En dépit du manque de législation,

217
00:12:38,733 --> 00:12:41,967
les caméras sont toujours en cours d'installation
dans les 800 emplacements annoncés,

218
00:12:41,992 --> 00:12:43,132
jusqu'alors inconnus.

219
00:12:43,950 --> 00:12:47,650
Cependant, des citoyennes & citoyens
se rassemblent autour de l'initiative #hiljadekamera

220
00:12:47,662 --> 00:12:49,900
et continuent de cartographier les emplacements.

221
00:12:50,100 --> 00:12:53,950
La lutte contre la surveillance de masse ne fait que débuter.

222
00:12:51,000 --> 00:12:53,990
{\an8}Réalisé par :

223
00:12:54,847 --> 00:12:59,904
{\an8}Montage :

224
00:13:01,234 --> 00:13:05,514
{\an8}Tournage :

225
00:13:07,436 --> 00:13:12,500
{\an8}Production / Assistance à la production / Musique :

226
00:13:14,554 --> 00:13:17,210
{\an8}Panorama Films for SHARE Foundation - 2020

227
00:13:14,554 --> 00:13:17,210
Sous-titres FR : P. pour Technopolice

